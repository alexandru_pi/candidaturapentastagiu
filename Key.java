package t5;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Key extends KeyAdapter{
	Player p;
	
	public Key(Player player){
		p=player;
	}
	
	public void keyPressed(KeyEvent e){
		p.KeyPressed(e);
	}
	
	public void keyReleased(KeyEvent e){
		p.KeyReleased(e);
	}
}
