package t5;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Joc extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	Timer mainTimer;
	Player player;
	
	int enemyCount = 1;
	public static int level = 1;
	
	static ArrayList<Racheta> rachete = new ArrayList<Racheta>();
	static ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	Random rand = new Random();
	
	public Joc(){	
		setFocusable(true);
		
		player=new Player(250,530);
		
		addKeyListener(new Key(player));
		
		mainTimer=new Timer(10,this);
		mainTimer.start();
		
		startGame();
	}
	
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		
		ImageIcon ic = new ImageIcon("C:/Users/Alex/Desktop/Back_tema.jpg");
		g2d.drawImage(ic.getImage(),0,0,null);
		
		player.draw(g2d);
		
		for(int i=0;i<enemies.size();++i)
		{
			Enemy temp =enemies.get(i);
			temp.draw(g2d);
		}
		
		for(int i=0;i<rachete.size();++i)
		{
			Racheta temp =rachete.get(i);
			temp.draw(g2d);
		}
	}

	
	public void actionPerformed(ActionEvent e){
		player.update();
		for(int i=0;i<enemies.size();i++)
		{
			Enemy temp = enemies.get(i);
			temp.update();
		}
		
		for(int i=0;i<rachete.size();++i)
		{
			Racheta temp =rachete.get(i);
			temp.update();
		}
		nivel();
		
		repaint();
	}
	
	public void addEnemy(Enemy e){
		enemies.add(e);
	}
	
	public static void addRacheta(Racheta r){
		rachete.add(r);
	}
	
	public static void removeRachete(Racheta r){
		rachete.remove(r);
	}
	
	public static ArrayList<Racheta> getRList(){
		return rachete;
	}
	
	public static void removeEnemy(Enemy e){
		enemies.remove(e);
	}
	
	public static ArrayList<Enemy> getEnemyList(){
		return enemies;
	}
	
	public void startGame(){
		enemyCount = level;
		
		for(int i=0;i<enemyCount;i++){
			addEnemy(new Enemy(rand.nextInt(400),0));
		}
	}
	
	public void nivel(){
		if(enemies.size()==0)
		{
			level++;
			JOptionPane.showMessageDialog(null,"Ati terminat nivelul "+(level-1));
			startGame();
		}
	}
}
