package t5;

import javax.swing.JFrame;

public class Main{
	
	public static void main(String[] args){
		JFrame frame=new JFrame("Joc Tema 5");
		frame.setSize(500,600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.add(new Joc());
		frame.setVisible(true);
	}
}
