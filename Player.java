package t5;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Player extends Obiecte{

	int vitX=0;
	int speed =2;
	
	public Player(int x, int y) {
		super(x, y);
	}
	
	public void update(){
		x += vitX;
		
		coliziuni();
	}
	
	public void draw(Graphics2D g2d){
		g2d.drawImage(getPlayerImage(), x, y, null);
		g2d.draw(getBounds());
	}
	
	public Image getPlayerImage(){
		ImageIcon ic= new ImageIcon("C:/Users/Alex/Desktop/player_ship.png");
		return ic.getImage();
	}
	
	public void KeyPressed(KeyEvent e){
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_A){
			vitX = -speed;
		}if(key == KeyEvent.VK_D){
			vitX = speed;
		}else if(key == KeyEvent.VK_SPACE){
			Joc.addRacheta(new Racheta(x, y));
		}
			
	}
	
	public void KeyReleased(KeyEvent e){
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_A){
			vitX= 0;
		}if(key == KeyEvent.VK_D){
			vitX = 0;
		}
			
	}
	
	public void coliziuni(){
		ArrayList<Enemy> enemies=Joc.getEnemyList();
		
		for(int i=0;i<enemies.size();++i){
			Enemy temp=enemies.get(i);
			if(getBounds().intersects(temp.getBounds()))
			{
				JOptionPane.showMessageDialog(null, "Ati pierdut !");
				System.exit(0);
			}
		}
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x,y,getPlayerImage().getWidth(null),
				getPlayerImage().getHeight(null));
	}
}
