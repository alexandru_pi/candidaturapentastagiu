package t5;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class Racheta extends Obiecte{

	public Racheta(int x, int y) {
		super(x, y);
	}
	
	public void update(){
		y -= 3;
	}
	
	public void draw(Graphics2D g2d){
		g2d.drawImage(getRImage(), x, y, null);
	}
	
	public Image getRImage(){
		ImageIcon ic= new ImageIcon("C:/Users/Alex/Desktop/SpaceInvaders/SpaceInvaders/DATA/ENEMY3.png");
		return ic.getImage();
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x,y,getRImage().getWidth(null),
				getRImage().getHeight(null));
	}
}
