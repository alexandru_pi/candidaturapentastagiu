package t5;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class Enemy extends Obiecte{
	private int starty=0;
	
	
	public Enemy(int x, int y) {
		super(x, y);
		starty=y;
	}
	
	public void update(){
		y+=1;
		coliziuni();
		iese();
	}
	
	public void draw(Graphics2D g2d){
		g2d.drawImage(getEnemyImage(), x, y, null);
		g2d.draw(getBounds());
	}
	
	public Image getEnemyImage(){
		ImageIcon ic= new ImageIcon("C:/Users/Alex/Desktop/SpaceInvaders/SpaceInvaders/DATA/ENEMY2.png");
		return ic.getImage();
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x,y,getEnemyImage().getWidth(null),
				getEnemyImage().getHeight(null));
	}
	
	public void iese(){
		if( y>= 600){
			y=starty;
		}
	}
	
	public void coliziuni(){
		for(int i=0;i<Joc.getRList().size();i++){
			Racheta r=Joc.getRList().get(i);
			
			if(getBounds().intersects(r.getBounds()))
			{
				Joc.removeEnemy(this);
				Joc.removeRachete(r);
			}
		}
	}
}
